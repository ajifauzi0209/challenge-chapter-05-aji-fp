package com.kazakimaru.ch05_ajifauzipangestu.service

import com.kazakimaru.ch05_ajifauzipangestu.model.MoviePopularResponse
import com.kazakimaru.ch05_ajifauzipangestu.model.Result
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TMDBApiService {
    @GET("movie/popular")
    fun getAllMovie(
        @Query("api_key") key: String
    ): Call<MoviePopularResponse>

    @GET("movie/{movie_id}")
    fun getDetailMovie(
        @Path("movie_id") movieId: Int,
        @Query("api_key") key: String
    ): Call<Result>
}