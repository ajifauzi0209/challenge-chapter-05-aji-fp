package com.kazakimaru.ch05_ajifauzipangestu.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.kazakimaru.ch05_ajifauzipangestu.AuthActivity
import com.kazakimaru.ch05_ajifauzipangestu.R
import com.kazakimaru.ch05_ajifauzipangestu.database.UserRepo
import com.kazakimaru.ch05_ajifauzipangestu.databinding.FragmentProfileBinding
import com.kazakimaru.ch05_ajifauzipangestu.helper.viewModelsFactory
import com.kazakimaru.ch05_ajifauzipangestu.viewmodel.HomeViewModel


class ProfileFragment : Fragment() {
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!

    private val userRepo: UserRepo by lazy { UserRepo(requireContext()) }
    private val sharedPrefs by lazy { context?.getSharedPreferences("SHARED_PREFS", Context.MODE_PRIVATE) }
    private val viewModel: HomeViewModel by viewModelsFactory { HomeViewModel(userRepo, sharedPrefs) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getUserDetail()
        updateUserProfile()
        doLogout()
        observeData()
    }


    private fun updateUserProfile() {
        binding.btnUpdate.setOnClickListener {
            // Get value dari EditText
            val etUsername = binding.editUsername.editText?.text.toString()
            val etNamaLengkap = binding.editNamaLengkap.editText?.text.toString()
            val etTglLahir = binding.editTglLahir.editText?.text.toString()
            val etAlamat = binding.editAlamat.editText?.text.toString()

            // Jalankan fungsi pada ViewModel
            viewModel.updateUserDetail(etUsername, etNamaLengkap, etTglLahir, etAlamat)
        }
    }

    private fun doLogout() {
        binding.btnLogout.setOnClickListener {
            viewModel.logoutAccount()
        }
    }

    private fun observeData() {
        viewModel.usernameDetail.observe(viewLifecycleOwner) {
            binding.editUsername.editText?.setText(it)
        }
        viewModel.namaLengkapDetail.observe(viewLifecycleOwner) {
            binding.editNamaLengkap.editText?.setText(it)
        }
        viewModel.tglLahirDetail.observe(viewLifecycleOwner) {
            binding.editTglLahir.editText?.setText(it)
        }
        viewModel.alamatDetail.observe(viewLifecycleOwner) {
            binding.editAlamat.editText?.setText(it)
        }
        viewModel.statusUpdateProfile.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), "Profile Berhasil Diperbarui", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_profileFragment_to_homeFragment)
        }
        viewModel.statusLogout.observe(viewLifecycleOwner) {
            val intent = Intent(requireContext(), AuthActivity::class.java)
            startActivity(intent)
            requireActivity().finish()
        }
    }

}